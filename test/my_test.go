package test

import (
	"testing"
	"awesomeProject/models"
	"github.com/stretchr/testify/assert"
)

func TestMakeSite(t *testing.T) {
	fakeLivre := models.MakeSite()
	if len(fakeLivre) != 2 {
		t.Errorf("error")
	}
}

func TestTwo(t *testing.T) {
	fakeLivre := models.MakeSite()
	nb := len(fakeLivre)
	assert := assert.New(t)
	assert.Equal(nb, 2, "the should be equal")
}