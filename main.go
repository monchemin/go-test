package main

import (
	"awesomeProject/models"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)




func homeHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(`{"message": "Hello world !"}`))

}

func AppRoute() *mux.Router{
	routes:= mux.NewRouter()
	routes.HandleFunc("/livre", models.GetLivre).Methods(http.MethodGet)
	routes.HandleFunc("/livres", models.GetSites)
	routes.HandleFunc("/", homeHandler)

	return routes
}


func main() {

	routes := AppRoute()
	log.Fatal(http.ListenAndServe(":8080", routes))

}
