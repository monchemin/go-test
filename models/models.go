package models

import (
	"encoding/json"
	. "net/http"
)

type Site struct {
	ID string `json:"iD"`
	Titre string `json:"title"`
	Devices *Device `json:"devices"`
}

type Device struct {
	ID string `json:"ID"`
	SN string `json:"SernialNumber"`
}


func GetLivre(w ResponseWriter, r *Request) {

}

func GetSites(w ResponseWriter, r *Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(MakeSite())
}

func MakeSite() []Site {
	livres := []Site{}
	livres = append(livres, Site{ID: "1",Titre:" Premier livre",
		Devices:&Device{ID: "John", SN: "Bina"}})
	livres = append(livres, Site{ID: "2", Titre:" Deuxième livre",
		Devices:&Device{ID: "John", SN: "Luo"}})
	return livres
}